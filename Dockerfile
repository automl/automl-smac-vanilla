FROM python:3.6.7

RUN apt-get update && apt-get upgrade -y

RUN apt-get install -y build-essential python3 python3-pip python3-dev git swig

ADD ./requirements.txt /app/requirements.txt

WORKDIR /app

RUN pip3 install -r requirements.txt

# NOTE: including ConfigSpace in requirements make a lot of problem
RUN python3 -m pip install ConfigSpace==0.4.6 smac==0.8.0

RUN python3 -m pip install matplotlib==3.1.1

COPY asv /app/asv
COPY setup.py /app
COPY setup.cfg /app
COPY misc/datasets.yaml /app
COPY misc/search_spaces/classifiers.json /app

RUN pip3 install .
